public class Producto_elementos {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4 };
        int product = productArray(arr);
        System.out.println(product); 
    }

    public static int productArray(int[] arr) {
        int product = 1;
        for (int i = 0; i < arr.length; i++) {
            product *= arr[i];
        }
        return product;
    }
}
