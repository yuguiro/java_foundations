import java.util.Scanner;

public class Leer_almacenar {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa el número (entre 1000 y 7000): ");
        int num = input.nextInt();

        if (num >= 1000 && num <= 7000) {
            int[] digits = almacenarDigitos(num);
            mostrarDigitos(digits);
        } else {
            System.out.println("El número ingresado no está en el rango válido.");
        }
    }

    public static int[] almacenarDigitos(int num) {
        int[] digits = new int[4];
        int i = 3;

        while (num > 0) {
            digits[i] = num % 10;
            num = num / 10;
            i--;
        }

        return digits;
    }

    public static void mostrarDigitos(int[] digits) {
        for (int i = 0; i < digits.length; i++) {
            System.out.println("[" + i + "] = " + digits[i]);
        }
    }
}
