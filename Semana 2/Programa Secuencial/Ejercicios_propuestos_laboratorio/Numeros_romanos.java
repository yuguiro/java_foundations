import java.util.Scanner;

public class Numeros_romanos {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number = getValidInput(input);
        String roman = convertToRoman(number);
        System.out.println("En números romanos es: " + roman);
    }

    public static int getValidInput(Scanner input) {
        int number;
        do {
            System.out.print("Ingresa un número: ");
            number = input.nextInt();
            if (number <= 0) {
                System.out.println("El número debe ser mayor que cero.");
            }
        } while (number <= 0);
        return number;
    }

    public static String convertToRoman(int number) {
        String[] romanSymbols = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int[] decimalValues = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < decimalValues.length; i++) {
            while (number >= decimalValues[i]) {
                number -= decimalValues[i];
                result.append(romanSymbols[i]);
            }
        }
        return result.toString();
    }
}
