import java.util.Scanner;

public class Alumnos_senati {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numStudents = 3; // valor según el número de estudiantes
        String[][] studentsData = new String[numStudents][2]; 

        for (int i = 0; i < numStudents; i++) {
            studentsData[i][0] = getStudentName(scanner, i+1);
            studentsData[i][1] = getStudentAge(scanner, i+1);
        }

        double averageAge = calculateAverageAge(studentsData);
        System.out.println("La edad promedio de los estudiantes es: " + averageAge);
    }

    public static String getStudentName(Scanner scanner, int numStudent) {
        System.out.println("Ingrese nombre del estudiante " + numStudent + ":");
        return scanner.nextLine();
    }

    public static String getStudentAge(Scanner scanner, int numStudent) {
        System.out.println("Ingrese edad del estudiante " + numStudent + ":");
        return scanner.nextLine();
    }

    public static double calculateAverageAge(String[][] studentsData) {
        int totalAge = 0;
        for (int i = 0; i < studentsData.length; i++) {
            totalAge += Integer.parseInt(studentsData[i][1]);
        }
        double averageAge = (double) totalAge / studentsData.length;
        return averageAge;
    }
}
