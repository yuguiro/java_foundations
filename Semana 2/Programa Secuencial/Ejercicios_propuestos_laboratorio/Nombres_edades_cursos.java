import java.util.ArrayList;
import java.util.Scanner;

public class Nombres_edades_cursos {
    public static void main(String[] args) {
        ArrayList<String> nombres = new ArrayList<>();
        ArrayList<Integer> edades = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String nombre = "";
        while (!nombre.equals("*")) {
            nombre = pedirNombre(scanner);
            if (!nombre.equals("*")) {
                nombres.add(nombre);
                int edad = pedirEdad(scanner);
                edades.add(edad);
            }
        }

        int mayor = buscarEdadMayor(edades);
        String nombreMayor = buscarNombreMayor(nombres, edades, mayor);
        System.out.println("Alumnos mayores de edad: ");
        imprimirAlumnosMayores(nombres, edades);
        System.out.printf("%s tiene la mayor edad %d años\n", nombreMayor, mayor);
    }
    
    public static String pedirNombre(Scanner scanner) {
        System.out.print("Ingresa el nombre del alumno: ");
        return scanner.nextLine();
    }
    
    public static int pedirEdad(Scanner scanner) {
        System.out.print("Ingresa su edad: ");
        return Integer.parseInt(scanner.nextLine());
    }
    
    public static int buscarEdadMayor(ArrayList<Integer> edades) {
        int mayor = edades.get(0);
        for (int i = 1; i < edades.size(); i++) {
            if (edades.get(i) > mayor) {
                mayor = edades.get(i);
            }
        }
        return mayor;
    }
    
    public static String buscarNombreMayor(ArrayList<String> nombres, ArrayList<Integer> edades, int mayor) {
        String nombreMayor = "";
        for (int i = 0; i < edades.size(); i++) {
            if (edades.get(i) == mayor) {
                nombreMayor = nombres.get(i);
                break;
            }
        }
        return nombreMayor;
    }
    
    public static void imprimirAlumnosMayores(ArrayList<String> nombres, ArrayList<Integer> edades) {
        for (int i = 0; i < edades.size(); i++) {
            if (edades.get(i) >= 18) {
                System.out.printf("%s tiene %d años\n", nombres.get(i), edades.get(i));
            }
        }
    }
}

