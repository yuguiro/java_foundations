import java.util.Scanner;

public class Metodo_arreglos_enteros {
    public static void main(String[] args) {
        int[] arr = { 5, 10, 2, 8, 35 };
        int maxElement = encontrarMayor(arr);
        System.out.println("El elemento máximo del arreglo es: " + maxElement);
    }

    public static int encontrarMayor(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
}
