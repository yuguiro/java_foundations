import java.util.Scanner;

public class Mensaje_segun_numero {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        while (num < 1 || num > 7) {
            System.out.print("Ingrese un número entre 1 y 7: ");
            num = scanner.nextInt();
            if (num < 1 || num > 7) {
                System.out.println("Número inválido, por favor intente de nuevo.");
            }
        }
        imprimirMensaje(num);
    }
    
    public static void imprimirMensaje(int num) {
        String mensaje = "";
        switch(num) {
            case 1:
                mensaje = "Hoy aprenderemos sobre programación";
                break;
            case 2:
                mensaje = "¿Qué tal tomar un curso de marketing digital?";
                break;
            case 3:
                mensaje = "Hoy es un gran día para comenzar a aprender de diseño";
                break;
            case 4:
                mensaje = "¿Y si aprendemos algo de negocios online?";
                break;
            case 5:
                mensaje = "Veamos un par de clases sobre producción audiovisual";
                break;
            case 6:
                mensaje = "Tal vez sea bueno desarrollar una habilidad blanda";
                break;
            case 7:
                mensaje = "Yo decido distraerme programando";
                break;
        }
        System.out.println(mensaje);
    }
}
