import java.util.Arrays;

public class Encontrar_mediana {
    public static void main(String[] args) {
        int[] arr = {7, 3, 1, 4, 2, 6, 5};
        double mediana = encontrarMediana(arr);
        System.out.println("La mediana es: " + mediana);
    }

    public static double encontrarMediana(int[] arr) {
        Arrays.sort(arr);

        int n = arr.length;
        if (n % 2 == 0) {
            return (double)(arr[n/2] + arr[n/2 - 1]) / 2;
        } else {
            return (double)arr[n/2];
        }
    }
}
