// Crear un arreglo de enteros y encontrar el subarreglo de longitud máxima en orden creciente.
import java.util.Arrays;

public class Arreglo_subarreglo {

    public static void main(String[] args) {
        int[] arreglo = {1, 2, 3, 1, 2, 3, 4, 5, 6, 1, 2};
        int[] subarreglo = subarregloCreciente(arreglo);

        System.out.println("Arreglo original: " + Arrays.toString(arreglo));
        System.out.println("Subarreglo creciente: " + Arrays.toString(subarreglo));
    }

    public static int[] subarregloCreciente(int[] arreglo) {
        int n = arreglo.length;
        int[] subarreglo = new int[n];
        int[] subarregloMax = new int[0];
        int longitud = 0;
        int longitudMax = 0;

        for (int i = 0; i < n; i++) {
            subarreglo[longitud] = arreglo[i];

            if (longitud == 0 || subarreglo[longitud] > subarreglo[longitud - 1]) {
                longitud++;

                if (longitud > longitudMax) {
                    longitudMax = longitud;
                    subarregloMax = Arrays.copyOf(subarreglo, longitudMax);
                }
            } else {
                longitud = 1;
                subarreglo[0] = arreglo[i];
            }
        }

        return subarregloMax;
    }
}


