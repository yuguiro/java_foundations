//Crear una matriz de 7x7 y encontrar la forma canónica de Jordan.

import java.util.Scanner;

public class Matriz_canonica {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 7; // tamaño de la matriz
        int[][] matriz = new int[n][n];

        llenarMatriz(matriz);

        System.out.println("Matriz original:");
        mostrarMatriz(matriz);

        int[][] jordan = formaJordan(matriz);

        System.out.println("Forma canónica de Jordan:");
        mostrarMatriz(jordan);
    }

    public static void llenarMatriz(int[][] matriz) {
        int n = matriz.length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matriz[i][j] = (int) (Math.random() * 10);
            }
        }
    }

    public static void mostrarMatriz(int[][] matriz) {
        int n = matriz.length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] formaJordan(int[][] matriz) {
        int n = matriz.length;
        int[][] jordan = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                jordan[i][j] = matriz[i][j];
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    jordan[i][j] = 0;
                }
            }
        }

        return jordan;
    }
}

