//Crear un arreglo de enteros y encontrar la moda.
import java.util.*;

public class Encontrar_la_moda {
    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5};
        int moda = encontrarModa(arr);
        System.out.println("La moda del arreglo es: " + moda);
    }

    public static int encontrarModa(int[] arr) {
        Map<Integer, Integer> frecuencia = new HashMap<>();
        int maxFrecuencia = 0;
        int moda = 0;

        for (int i : arr) {
            int f = frecuencia.getOrDefault(i, 0) + 1;
            frecuencia.put(i, f);
            if (f > maxFrecuencia) {
                maxFrecuencia = f;
                moda = i;
            }
        }

        return moda;
    }
}


