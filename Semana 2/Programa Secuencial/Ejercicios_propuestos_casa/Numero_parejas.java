// Crear un arreglo de enteros y encontrar el número de parejas de elementos cuya suma es un número primo.
import java.util.Scanner;

public class Numero_parejas {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el tamaño del arreglo: ");
        int size = input.nextInt();

        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = (int) (Math.random() * 100 + 1);
        }

        System.out.println("Arreglo: ");
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();

        System.out.print("Ingrese un número para encontrar parejas de elementos cuya suma es un número primo: ");
        int target = input.nextInt();

        int count = findPairs(arr, target);

        System.out.println("El número de parejas de elementos cuya suma es un número primo es: " + count);
    }

    public static int findPairs(int[] arr, int target) {
        int count = 0;

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                int sum = arr[i] + arr[j];
                if (isPrime(sum) && sum == target) {
                    count++;
                }
            }
        }

        return count;
    }

    public static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }

        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }
}


