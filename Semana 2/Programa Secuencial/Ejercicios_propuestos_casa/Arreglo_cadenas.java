// Crear un arreglo de cadenas y ordenarlo en orden lexicográfico inverso.
import java.util.Arrays;
import java.util.Collections;

public class Arreglo_cadenas {
    public static void main(String[] args) {
        String[] arr = {"senati", "institucion", "creada", "objetivo", "proporcionar", "formacion"};

        ordenarInverso(arr);

        System.out.println(Arrays.toString(arr));
    }

    public static void ordenarInverso(String[] arr) {
        Arrays.sort(arr, Collections.reverseOrder());
    }
}


