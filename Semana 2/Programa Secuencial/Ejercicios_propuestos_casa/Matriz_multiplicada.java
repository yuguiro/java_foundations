// Crear una matriz de 5x5 y multiplicarla por otra matriz de 3x3.
import java.util.Random;

public class Matriz_multiplicada {

    public static void main(String[] args) {
        int[][] matriz1 = crearMatriz(5, 5);
        int[][] matriz2 = crearMatriz(3, 3);
        int[][] matrizResultado = multiplicarMatrices(matriz1, matriz2);

        System.out.println("Matriz 1:");
        imprimirMatriz(matriz1);

        System.out.println("\nMatriz 2:");
        imprimirMatriz(matriz2);

        System.out.println("\nMatriz Resultado:");
        imprimirMatriz(matrizResultado);
    }

    public static int[][] crearMatriz(int filas, int columnas) {
        int[][] matriz = new int[filas][columnas];
        Random random = new Random();

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                matriz[i][j] = random.nextInt(100) + 1;
            }
        }

        return matriz;
    }

    public static int[][] multiplicarMatrices(int[][] matriz1, int[][] matriz2) {
        int m = matriz1.length;
        int n = matriz2[0].length;
        int[][] resultado = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                resultado[i][j] = multiplicarFilaYColumna(matriz1, i, matriz2, j);
            }
        }

        return resultado;
    }

    public static int multiplicarFilaYColumna(int[][] matriz1, int fila, int[][] matriz2, int columna) {
        int sum = 0;

        for (int i = 0; i < matriz2.length; i++) {
            sum += matriz1[fila][i] * matriz2[i][columna];
        }

        return sum;
    }

    public static void imprimirMatriz(int[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}
