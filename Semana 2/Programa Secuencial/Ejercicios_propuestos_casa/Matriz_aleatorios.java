//Crear una matriz de 7x7 y llenarla con números aleatorios del 1 al 100.
import java.util.Random;

public class Matriz_aleatorios {
    
    public static void main(String[] args) {
        int[][] matriz = generarMatriz();
        mostrarMatriz(matriz);
    }
    
    public static int[][] generarMatriz() {
        int[][] matriz = new int[7][7];
        Random rand = new Random();
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                matriz[i][j] = rand.nextInt(100) + 1;
            }
        }
        return matriz;
    }
    
    public static void mostrarMatriz(int[][] matriz) {
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}


