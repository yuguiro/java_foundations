// Crear una matriz de 7x7 y rotarla 90 grados en sentido horario.
public class Matriz_rotada {
    
    public static void main(String[] args) {
        int[][] matriz = generarMatriz();
        System.out.println("Matriz original:");
        mostrarMatriz(matriz);
        int[][] matrizRotada = rotarMatriz(matriz);
        System.out.println("Matriz rotada 90 grados en sentido horario:");
        mostrarMatriz(matrizRotada);
    }
    
    public static int[][] generarMatriz() {
        int[][] matriz = new int[7][7];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                matriz[i][j] = i + j;
            }
        }
        return matriz;
    }
    
    public static void mostrarMatriz(int[][] matriz) {
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    public static int[][] rotarMatriz(int[][] matriz) {
        int[][] matrizRotada = new int[7][7];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                matrizRotada[i][j] = matriz[6-j][i];
            }
        }
        return matrizRotada;
    }
}
