// Crear un arreglo de enteros y encontrar el k-ésimo elemento más grande.
public class Arreglo_enteros {
    public static void main(String[] args) {
        int[] arr = {3, 2, 1, 5, 6, 4};
        int k = 2;
        int kthLargest = findKthLargest(arr, k);
        System.out.println("El " + k + "-ésimo elemento más grande es: " + kthLargest);
    }

    public static int findKthLargest(int[] nums, int k) {
        int left = 0;
        int right = nums.length - 1;

        while (true) {
            int pivotIndex = partition(nums, left, right);
            if (pivotIndex == k - 1) {
                return nums[pivotIndex];
            } else if (pivotIndex < k - 1) {
                left = pivotIndex + 1;
            } else {
                right = pivotIndex - 1;
            }
        }
    }

    private static int partition(int[] nums, int left, int right) {
        int pivotValue = nums[right];
        int pivotIndex = left;

        for (int i = left; i < right; i++) {
            if (nums[i] > pivotValue) {
                swap(nums, i, pivotIndex);
                pivotIndex++;
            }
        }

        swap(nums, pivotIndex, right);
        return pivotIndex;
    }

    private static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}



