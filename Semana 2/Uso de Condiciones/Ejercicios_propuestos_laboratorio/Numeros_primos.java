import java.util.Scanner;

public class Numeros_primos {

    public static void main(String[] args) {
        int N, i, j;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("Ingrese un numero n: ");
            N = sc.nextInt();
        } while (N <= 0);
        System.out.println("inicio de primos desde 2 hasta " + N);

        for (j = 2; j <= N; j++) {    
            i = 2;                
            while (j % i != 0) {
                i++;
            }
            if (i == j) {                   
                System.out.println(j);
            }    
           
        }
    }
}