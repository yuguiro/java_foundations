import java.util.Scanner;

public class Suma_números_impares{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese el numero del valor de N: ");
        int N = input.nextInt();
        int suma = 0;
        for (int i = 1; i <= N; i++) {
            suma+= 2 * i - 1;
        }
        System.out.println("La cantidad de numeros impares que quieres sumar " + N + " suma de todos los impares es: Rpta " + suma);
    }
}