import java.util.Scanner;
public class Minimo_comun_multiplo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("introduzca el primer numero: ");
        int a = sc.nextInt();
        System.out.print("introduzca el segundo numero: ");
        int b = sc.nextInt();
        int lcm = (a * b) / gcd(a, b);
        System.out.println("Su minimo común multiplo es 'MCD' = " + lcm);
    }

    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return gcd(b, a % b);
        }
    }
}