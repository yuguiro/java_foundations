import java.util.Scanner;

public class Calcular_bucle {
    public static void main(String[] args) {
        int num, factorial = 1, i = 1;
        Scanner sc = new Scanner(System.in);
        System.out.print("Introducir un número: ");
        num = sc.nextInt();
        while (i <= num) {
            factorial *= i;
            i++;
        }
        System.out.println("Su factorial de " + num + " es: " + factorial);
    }
}