import java.util.Scanner;
public class Maximo_comun_divisor {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);   
    System.out.print("Cuantos numeros quieres calcular: ");
    int n = input.nextInt();

    int[] numeros = new int[n];
    
    for (int i = 0; i < n; i++) {
      System.out.print("Ingresa el número " + (i+1) + ": ");
      numeros[i] = input.nextInt();
    }
    int mcd = numeros[0];
    int i = 1;
    while (i < n) {
      int a = mcd;
      int b = numeros[i];
      while (b != 0) {
        int temp = b;
        b = a % b;
        a = temp;
      }
      mcd = a;
      i++;
    }
    
    System.out.println("El MaximoComúnDivisor de los números ingresados es: " + mcd);
  }
}