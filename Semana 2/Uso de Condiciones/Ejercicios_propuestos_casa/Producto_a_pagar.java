public class Producto_a_pagar {
    public static void main(String[] args) {
        double montoInicial = 10.0; // primer mes
        double montoTotal = 0.0; // total a pagar después de los 20 meses
        int numMeses = 20; // número de meses 
        
        for (int i = 0; i < numMeses; i++) {
            double montoMensual = montoInicial * Math.pow(2, i);
            System.out.println("Monto a pagar en el mes " + (i+1) + ": S/" + montoMensual);
            montoTotal += montoMensual;
        }

        System.out.println("Monto total a pagar de los 20 meses: S/" + montoTotal);
    }
}
