import java.util.Scanner;

public class Cronometro {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de segundos a contar: ");
        int segundos = sc.nextInt();

        for (int i = segundos; i >= 0; i--) {
            System.out.print("\r" + i);
            try {
                Thread.sleep(1000); 
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("\nCronómetro finalizado.");
        sc.close();
    }
}
