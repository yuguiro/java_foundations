import java.util.Scanner;

public class Metodo_insercion {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el tamaño de la matriz: ");
        int tam = input.nextInt();

        int[] numeros = new int[tam];

        System.out.println("Ingrese los valores de la matriz:");
        for (int i = 0; i < tam; i++) {
            System.out.print("Valor " + (i + 1) + ": ");
            numeros[i] = input.nextInt();
        }

        for (int i = 1; i < numeros.length; i++) {
            int j = i - 1;
            int temp = numeros[i];
            while (j >= 0 && numeros[j] > temp) {
                numeros[j + 1] = numeros[j];
                j--;
            }
            numeros[j + 1] = temp;
        }

        System.out.println("Matriz ordenada:");
        for (int i = 0; i < tam; i++) {
            System.out.print(numeros[i] + " ");
        }
    }
}
