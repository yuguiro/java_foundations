public class Valores_enteros {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            int factorial = 1;
            
            for (int j = 2; j <= i; j++) {
                factorial *= j;
            }
            
            System.out.println("El factorial de " + i + " es " + factorial);
        }
    }
}
