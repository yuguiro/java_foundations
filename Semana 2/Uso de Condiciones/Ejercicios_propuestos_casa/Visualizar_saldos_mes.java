public class Visualizar_saldos_mes {
    public static void main(String[] args) {
        double prestamo = 5000;
        double tasa_interes = 0.016;
        int meses = 18;
        double pago_mensual = prestamo / meses;

        for (int i = 1; i <= meses; i++) {
            double saldo = prestamo * Math.pow(1 + tasa_interes, i) - pago_mensual * ((Math.pow(1 + tasa_interes, i) - 1) / tasa_interes);
            System.out.printf("Mes %d: S/ %.2f\n", i, saldo);
        }
    }
}
