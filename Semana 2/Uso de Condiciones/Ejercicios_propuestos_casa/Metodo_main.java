import java.util.Scanner;

public class Metodo_main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce el texto: ");
        String cadena = sc.nextLine();
        
        int i = 0;
        while (i < cadena.length()) {
            int indice = i + 1;
            System.out.println("Letra " + indice + ": " + cadena.substring(i, i+1));
            i++;
        }
    }
}

