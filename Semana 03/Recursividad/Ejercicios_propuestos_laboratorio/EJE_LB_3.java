import java.io.*;
import java.util.*;

class MCM {
    
    public static int calcularMCD(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calcularMCD(b, a % b);
        }
    }
    
    public static int calcularMCM(int a, int b) {
        return (a * b) / calcularMCD(a, b);
    }
    
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números a calcular: ");
        int n = sc.nextInt();
        
        FileWriter archivoDatos = new FileWriter("datos_generados.txt");
        Random rand = new Random();
        int rango = 301; 
        for (int i = 0; i < n; i++) {
            int dato = rand.nextInt(rango);
            archivoDatos.write(Integer.toString(dato) + "\n");
        }
        archivoDatos.close();
        
        FileReader archivo = new FileReader("datos_generados.txt");
        BufferedReader lector = new BufferedReader(archivo);
        ArrayList<Integer> datos = new ArrayList<Integer>();
        String linea;
        while ((linea = lector.readLine()) != null) {
            int dato = Integer.parseInt(linea);
            datos.add(dato);
        }
        lector.close();
        
        int mcm = datos.get(0);
        for (int i = 1; i < n; i++) {
            mcm = calcularMCM(mcm, datos.get(i));
        }
        
        FileWriter archivoResultados = new FileWriter("resultados.txt");
        archivoResultados.write("El MCM de los " + datos + " numeros generados aleatoriamente es " + mcm );
        archivoResultados.write("\n Autor:Yuguiro Suni");
        archivoResultados.close();
    }
}

