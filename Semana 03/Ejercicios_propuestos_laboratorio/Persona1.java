public class Persona1 {
    String nombre;
    String apellidos;
    String númeroDocumentoIdentidad;
    int añoNacimiento;
    String paísNacimiento;
    char género;

    Persona1(String nombre, String apellidos, String númeroDocumentoIdentidad, int añoNacimiento, String paísNacimiento, char género) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.númeroDocumentoIdentidad = númeroDocumentoIdentidad;
        this.añoNacimiento = añoNacimiento;
        this.paísNacimiento = paísNacimiento;
        this.género = género;
    }

    void imprimir() {
        System.out.println("Nombres: " + nombre);
        System.out.println("Apellido: " + apellidos);
        System.out.println("Número de documento de identidad: " + númeroDocumentoIdentidad);
        System.out.println("Año de nacimiento: " + añoNacimiento);
        System.out.println("País donde nacio: " + paísNacimiento);
        System.out.println("Género: " + género);
    }

    public static void main(String[] args) {
        Persona1 persona1 = new Persona1("Yuguiro", "Suni Benito", "73451520", 1993, "Perú", 'H');
        Persona1 persona2 = new Persona1("Joselin", "Pari Flores", "46653213", 1990, "Perú", 'M');

        System.out.println("=============Persona1=============");
        persona1.imprimir();

        System.out.println();

        System.out.println("=============Persona2=============");
        persona2.imprimir();
    }
}
