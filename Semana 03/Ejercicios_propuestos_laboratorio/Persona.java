/**

* Esta clase define objetos de tipo Persona con un nombre, apellidos,
* número de documento de identidad y año de nacimiento.
*
* @version 1.2/2023
 */
public class Persona {
    String nombre; // Atributo que identifica el nombre de una persona
    String apellidos; // Atributo que identifica los apellidos de una persona
    /*
  * Atributo que identifica el número de documento de identidad de
  * una persona
     */
    String númeroDocumentoIdentidad;
    int añoNacimiento; /*
                        *Atributo que identifica el año de nacimiento
                        * de una persona
                        */

    /**
  * Constructor de la clase Persona
  * @param nombre Parámetro que define el nombre de la persona
  * @param apellidos Parámetro que define los apellidos de la persona
  * @param númeroDocumentoIdentidad Parámetro que define el
  * número del documento de identidad de la persona
  * @param añoNacimiento Parámetro que define el año de nacimiento
  * de la persona
    */
    Persona(String nombre, String apellidos, String númeroDocumentoIdentidad, int añoNacimiento) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.númeroDocumentoIdentidad = númeroDocumentoIdentidad;
        this.añoNacimiento = añoNacimiento;

    }
    void imprimir (){
        System.out.println("Nombre: " + nombre);
        System.out.println("Apellidos: " + apellidos);
        System.out.println("DNI: " + númeroDocumentoIdentidad);
        System.out.println("Año de nacimiento: "+añoNacimiento);
        System.out.println();
    }

    public static void main(String[] args) {
        Persona persona01 = new Persona ("Yuguiro" , "Suni" , "73451520" , 1993);
        Persona persona02 = new Persona ("Jorge" , "Pari" , "73421521" , 2000);
        persona01.imprimir();
        persona02.imprimir();
    
    }
}
