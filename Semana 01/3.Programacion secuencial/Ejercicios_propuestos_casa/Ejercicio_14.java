import java.util.Scanner;

public class Ejercicio_14 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double costoProducto, impuesto, descuento, precioVenta;

        System.out.print("Ingrese el costo del producto: ");
        costoProducto = sc.nextDouble();

        System.out.print("Ingrese el porcentaje de impuesto (en decimal): ");
        impuesto = sc.nextDouble();

        System.out.print("Ingrese el porcentaje de descuento (en decimal): ");
        descuento = sc.nextDouble();

        // Cálculo del precio de venta
        precioVenta = costoProducto + (costoProducto * impuesto) - (costoProducto * descuento);

        System.out.println("El precio de venta del producto es: " + precioVenta);
    }
}
