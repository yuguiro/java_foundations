import java.util.Scanner;

public class Ejercicio_19 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n, num, contador = 0;

        System.out.print("Ingrese una secuencia de números separados por espacios: ");
        String entrada = sc.nextLine(); 
        String[] numeros = entrada.split(" "); 
        for (String numero : numeros) {

            n = Integer.parseInt(numero);

            num = n % 10;

            if (num == 2) {
                contador++;
            }
        }

        System.out.println("Cantidad de números terminados en 2: " + contador);
    }
}


