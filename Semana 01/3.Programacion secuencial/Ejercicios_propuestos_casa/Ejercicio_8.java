//formula para calcular el volumen de la esfera v = (4/3)PIr^3.

import java.util.*;

public class Ejercicio_8 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double V,r,d;
        System.out.print("Dame el radio");
   
        r=sc.nextDouble();
        d=4.0/3;
        V = ((d)* Math.PI * Math.pow(r,3));
               
        System.out.print("El volumen es " + V);
   
    }
}