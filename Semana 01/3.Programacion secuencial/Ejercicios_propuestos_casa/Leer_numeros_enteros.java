import java.util.Scanner;

public class Leer_numeros_enteros {
    public static void main(String[] args) {
        // Creamos un objeto Scanner para leer la entrada del teclado
        Scanner scanner = new Scanner(System.in);

        System.out.println("Por favor, ingrese el primer número entero: ");
        // Leemos el primer número entero
        int numero1 = scanner.nextInt();

        System.out.println("Por favor, ingrese el segundo número entero: ");
        // Leemos el segundo número entero
        int numero2 = scanner.nextInt();

        // Imprimimos los números por pantalla
        System.out.println("El primer número ingresado es: " + numero1);
        System.out.println("El segundo número ingresado es: " + numero2);

        // Cerrar el objeto Scanner
        scanner.close();
    }
}
