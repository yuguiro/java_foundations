import java.util.Scanner;

public class Ejercicio_17 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char repetir;
        do {
            System.out.print("Ingrese la temperatura en grados centígrados: ");
            double centigrados = sc.nextDouble();
            double kelvin = convertirACelvin(centigrados);
            System.out.println("Temperatura en grados Kelvin: " + kelvin);
            System.out.print("Repetir proceso? (S/N): ");
            repetir = sc.next().charAt(0);
        } while (repetir == 'S' || repetir == 's');
    }

    public static double convertirACelvin(double centigrados) {
        return centigrados + 273.15;
    }
}
