import java.util.Scanner;

public class Programa_grados_centigrados {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Por favor, ingrese la cantidad de grados centígrados: ");
        double centigrados = scanner.nextDouble();
        double fahrenheit = 32 + (9 * centigrados / 5);
        System.out.println(centigrados + " grados centígrados equivale a " + fahrenheit + " grados Fahrenheit.");
        scanner.close();
    }
}
