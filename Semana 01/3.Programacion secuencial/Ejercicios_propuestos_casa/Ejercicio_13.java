import java.util.Scanner;

public class Ejercicio_13 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int dia, año, suerte, suma, cifra1, cifra2, cifra3, cifra4;
        String mes;
        String[] nombresMeses = {"enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"};
        System.out.println("Introduzca fecha de nacimiento");
        System.out.print("Día: ");
        dia = sc.nextInt();
        System.out.print("Mes (en letras): ");
        mes = sc.next().toLowerCase(); 
        System.out.print("Año: ");
        año = sc.nextInt();
        suma = dia + obtenerValorMes(mes) + año; 
        cifra1 = suma/1000;      //obtiene la primera cifra
        cifra2 = suma/100%10;    //obtiene la segunda cifra
        cifra3 = suma/10%10;     //obtiene la tercera cifra
        cifra4 = suma%10;        //obtiene la última cifra
        suerte = cifra1 + cifra2 + cifra3 + cifra4;
        System.out.println("Su número de la suerte es: " + suerte);
        System.out.println("Fecha de nacimiento: " + dia + " de " + nombresMeses[obtenerValorMes(mes)-1] + " del " + año);
    }

    // Método para obtener el valor del mes en números
    public static int obtenerValorMes(String mes) {
        switch (mes) {
            case "enero":
                return 1;
            case "febrero":
                return 2;
            case "marzo":
                return 3;
            case "abril":
                return 4;
            case "mayo":
                return 5;
            case "junio":
                return 6;
            case "julio":
                return 7;
            case "agosto":
                return 8;
            case "septiembre":
                return 9;
            case "octubre":
                return 10;
            case "noviembre":
                return 11;
            case "diciembre":
                return 12;
            default:
                return -1; // Valor de retorno para casos inválidos
        }
    }
}
