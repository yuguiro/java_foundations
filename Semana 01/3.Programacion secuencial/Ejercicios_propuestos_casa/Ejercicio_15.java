import java.util.Scanner;

public class Ejercicio_15 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int n = sc.nextInt();
        System.out.print("Ingrese la cantidad de cifras a quitar: ");
        int m = sc.nextInt();

        int resultado = quitarUltimasCifras(n, m);
        System.out.println("El número después de quitar las últimas " + m + " cifras es: " + resultado);
    }

    public static int quitarUltimasCifras(int numero, int cifras) {
        String numeroStr = Integer.toString(numero);
        if (numeroStr.length() <= cifras) {
            return 0;
        }
        String resultadoStr = numeroStr.substring(0, numeroStr.length() - cifras);
        int resultado = Integer.parseInt(resultadoStr);
        return resultado;
    }
}
