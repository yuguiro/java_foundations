import java.util.Scanner;

public class Saludo_personal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Por favor, ingrese su nombre: ");
        String nombre = scanner.nextLine();
        System.out.println("Buenos días, " + nombre + "!");
        scanner.close();
    }
}
