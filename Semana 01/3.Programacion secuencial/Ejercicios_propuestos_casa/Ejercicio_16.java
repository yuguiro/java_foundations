import java.util.Scanner;

public class Ejercicio_16 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la temperatura en grados centígrados: ");
        double centigrados = sc.nextDouble();

        double kelvin = convertirACelvin(centigrados);
        double reamur = convertirAReamur(centigrados);

        System.out.println("Temperatura en grados Kelvin: " + kelvin);
        System.out.println("Temperatura en grados Reamur: " + reamur);
    }

    public static double convertirACelvin(double centigrados) {
        return centigrados + 273.15;
    }

    public static double convertirAReamur(double centigrados) {
        return centigrados * 0.8;
    }
}
