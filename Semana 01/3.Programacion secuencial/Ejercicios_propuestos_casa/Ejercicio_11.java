import java.util.Scanner;

public class Ejercicio_11 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Por favor, ingrese un número entero de 5 cifras: ");
        int numero = scanner.nextInt();
        String numeroStr = Integer.toString(numero);
        int longitud = numeroStr.length();

        if (longitud != 5) {
            System.out.println("El número ingresado no tiene 5 cifras.");
        } else {

            System.out.println("Las cifras del número desde el principio son:");
            for (int i = 0; i < longitud; i++) {
                char cifra = numeroStr.charAt(i);
                System.out.print(cifra + " ");
            }
        }
        scanner.close();
    }
}
