import java.util.Scanner;

public class Valor_absoluto {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int numero = scanner.nextInt();
        scanner.close();

        int valorAbsoluto = Math.abs(numero);

        System.out.println("El valor absoluto de " + numero + " es: " + valorAbsoluto);
    }
}
