import java.util.Scanner;

public class Numero_primo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int numero = scanner.nextInt();
        scanner.close();

        if (esPrimo(numero)) {
            System.out.println(numero + " si es un número primo.");
        } else {
            System.out.println(numero + " no es un número primo.");
        }
    }

    // Método para verificar si un número es primo
    public static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }

        // Verificar si el número es divisible por algún número menor que él
        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false; // No es primo
            }
        }

        return true; // Es primo
    }
}
