import java.util.Scanner;

public class Área_triangulo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese la longitud de los tres lados del triángulo: ");
        System.out.print("Medida Lado 1: ");
        double lado1 = scanner.nextDouble();
        System.out.print("Medida Lado 2: ");
        double lado2 = scanner.nextDouble();
        System.out.print("Medida Lado 3: ");
        double lado3 = scanner.nextDouble();
        scanner.close();

        double area = calcularAreaTriangulo(lado1, lado2, lado3);

        if (area != -1) {
            System.out.println("El área del triángulo es: " + area);
        } else {
            System.out.println("Los lados ingresados no forman un triángulo válido.");
        }
    }

   
    public static double calcularAreaTriangulo(double lado1, double lado2, double lado3) {
        if (lado1 + lado2 > lado3 && lado1 + lado3 > lado2 && lado2 + lado3 > lado1) {
            double semiperimetro = (lado1 + lado2 + lado3) / 2;
            double area = Math.sqrt(semiperimetro * (semiperimetro - lado1) *
                    (semiperimetro - lado2) * (semiperimetro - lado3));
            return area;
        } else {
            return -1; // Indicar que los lados no forman un triángulo válido
        }
    }
}
