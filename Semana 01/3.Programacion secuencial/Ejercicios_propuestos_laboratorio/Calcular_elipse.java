import java.util.Scanner;

public class Calcular_elipse {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingresa el valor del semieje mayor (a): ");
        double a = input.nextDouble(); // Leer el valor del semieje mayor
        
        System.out.print("Ingresa el valor del semieje menor (b): ");
        double b = input.nextDouble(); // Leer el valor del semieje menor
        
        // Calcular el área de la elipse
        double area = Math.PI * a * b;
        
        // Calcular el perímetro de la elipse
        double perimetro = 2 * Math.PI * Math.sqrt((a * a + b * b) / 2);
        
        // Imprimir resultados
        System.out.println("El área de la elipse es: " + area);
        System.out.println("El perímetro de la elipse es: " + perimetro);
    }
}
