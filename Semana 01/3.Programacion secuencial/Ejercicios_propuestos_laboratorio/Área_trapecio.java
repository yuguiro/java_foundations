import java.util.Scanner;

public class Área_trapecio {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese la longitud de los cuatro lados del trapecio: ");
        System.out.print("Medida Lado 1: ");
        double lado1 = scanner.nextDouble();
        System.out.print("Medida Lado 2: ");
        double lado2 = scanner.nextDouble();
        System.out.print("Medida Lado 3: ");
        double lado3 = scanner.nextDouble();
        System.out.print("Medida Lado 4: ");
        double lado4 = scanner.nextDouble();
        System.out.print("Medida Altura: ");
        double altura = scanner.nextDouble();
        scanner.close();

        double area = calcularAreaTrapecio(lado1, lado2, lado3, lado4, altura);

        System.out.println("El área del trapecio es: " + area);
    }

    public static double calcularAreaTrapecio(double lado1, double lado2, double lado3, double lado4, double altura) {
        // Calcular el promedio de las bases
        double basePromedio = (lado1 + lado2) / 2;
        // Calcular el área del trapecio
        double area = basePromedio * altura;
        return area;
    }
}
