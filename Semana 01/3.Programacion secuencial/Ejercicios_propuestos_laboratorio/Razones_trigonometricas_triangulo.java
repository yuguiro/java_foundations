import java.util.Scanner;

public class Razones_trigonometricas_triangulo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese las longitudes de los tres lados del triángulo: ");
        System.out.print("Medida Lado 1: ");
        double lado1 = scanner.nextDouble();
        System.out.print("Medida Lado 2: ");
        double lado2 = scanner.nextDouble();
        System.out.print("Medida Lado 3: ");
        double lado3 = scanner.nextDouble();
        scanner.close();

        boolean esTrianguloRectangulo = verificarTrianguloRectangulo(lado1, lado2, lado3);

        if (esTrianguloRectangulo) {
            System.out.println("Los lados ingresados forman un triángulo rectángulo.");
            double hipotenusa;
            double cateto1;
            double cateto2;
            if (lado1 > lado2 && lado1 > lado3) {
                hipotenusa = lado1;
                cateto1 = lado2;
                cateto2 = lado3;
            } else if (lado2 > lado1 && lado2 > lado3) {
                hipotenusa = lado2;
                cateto1 = lado1;
                cateto2 = lado3;
            } else {
                hipotenusa = lado3;
                cateto1 = lado1;
                cateto2 = lado2;
            }
            double seno = cateto1 / hipotenusa;
            double coseno = cateto2 / hipotenusa;
            double tangente = cateto1 / cateto2;
            System.out.println("Seno: " + seno);
            System.out.println("Coseno: " + coseno);
            System.out.println("Tangente: " + tangente);
        } else {
            System.out.println("Los lados ingresados no forman un triángulo rectángulo.");
        }
    }

    // Método para verificar si los lados forman un triángulo rectángulo
    public static boolean verificarTrianguloRectangulo(double lado1, double lado2, double lado3) {
        // Ordenar los lados de menor a mayor
        double hipotenusa;
        double cateto1;
        double cateto2;
        if (lado1 > lado2 && lado1 > lado3) {
            hipotenusa = lado1;
            cateto1 = lado2;
            cateto2 = lado3;
        } else if (lado2 > lado1 && lado2 > lado3) {
            hipotenusa = lado2;
            cateto1 = lado1;
            cateto2 = lado3;
        } else {
            hipotenusa = lado3;
            cateto1 = lado1;
            cateto2 = lado2;
        }

        // Verificar si los lados forman un triángulo rectángulo
        return hipotenusa * hipotenusa == cateto1 * cateto1 + cateto2 * cateto2;
    }
}
