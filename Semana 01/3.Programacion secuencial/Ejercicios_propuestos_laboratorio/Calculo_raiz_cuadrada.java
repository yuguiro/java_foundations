public class Calculo_raiz_cuadrada {
    public static void main(String[] args) {
        int num = 3; // Número entero
        
        // Calcular la raíz cuadrada
        double raizCuadrada = Math.sqrt(num);
        
        System.out.println("La raíz cuadrada de " + num + " es: " + raizCuadrada);
    }
}
