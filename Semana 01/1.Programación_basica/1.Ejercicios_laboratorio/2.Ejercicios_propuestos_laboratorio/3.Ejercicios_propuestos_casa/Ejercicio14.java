import java.math.BigDecimal;

public class Ejercicio14 {
    public static void main(String[] args) {
        // Creamos dos objetos BigDecimal con los números a sumar
        BigDecimal num1 = new BigDecimal("159753456852");
        BigDecimal num2 = new BigDecimal("951753456258");

        // Realizamos algunas operaciones basicas de matematicas con BigDecimal
        BigDecimal resultado = num1.add(num2);
        BigDecimal resultadoResta = num1.subtract(num2);
        BigDecimal resultadoMultiplicacion = num1.multiply(num2);
        BigDecimal resultadoDivision = num1.divide(num2, 10, BigDecimal.ROUND_HALF_UP);

        // Imprimimos los resultados
        System.out.println("La Suma de : " + num1 + "  y  " + num2 + " = " + resultado);
        System.out.println("La Resta de : " + num1 + "  y  " + num2 + " = " + resultadoResta);
        System.out.println("La Multiplicación de : " + num1 + "  y  " + num2 + " = " + resultadoMultiplicacion);    
        System.out.println("La División de : " + num1 + "  y  " + num2 + " = " + resultadoDivision);
    }
}
