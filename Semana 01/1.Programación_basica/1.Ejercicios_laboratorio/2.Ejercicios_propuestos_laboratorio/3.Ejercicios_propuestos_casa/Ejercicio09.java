public class Ejercicio09 {
    public static void main(String[] args) {
        int num1 = 10;
        int num2 = 5;

        // Realizamos algunas operaciones basicas de matematicas
        int suma = Math.addExact(num1, num2);        
        int resta = Math.subtractExact(num1, num2);        
        int multiplicacion = Math.multiplyExact(num1, num2);       
        double division = (double) num1 / num2;
       
        // Imprimimos los resultados
        System.out.println("La suma de: " + num1 + " + " + num2 + " = " + suma);
        System.out.println("La resta de: " + num1 + " - " + num2 + " = " + resta);
        System.out.println("La multiplicación de: " + num1 + " * " + num2 + " = " + multiplicacion);
        System.out.println("La división de: " + num1 + " / " + num2 + " = " + division);

    }
}
