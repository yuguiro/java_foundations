import java.text.SimpleDateFormat;
import java.util.Date;

public class Ejercicio13 {
    public static void main(String[] args) {
        // Creamos un objeto SimpleDateFormat con el formato deseado
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");

        // Obtenemos la fecha y hora actual
        Date fechaHoraActual = new Date();

        // Formatear la fecha y hora actual
        String fechaHoraFormateada = formato.format(fechaHoraActual);

        // Imprimimos la fecha y hora formateada
        System.out.println("La fecha y hora actual es: " + fechaHoraFormateada);
    }
}