import java.net.*;

public class Ejercicio20 {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 8080);
            System.out.println("Se ha establecido la conexión con el servidor.");
            socket.close();
        } catch (Exception e) {
            System.out.println("Se ha producido un error al establecer la conexión con el servidor.");
            e.printStackTrace();
        }
    }
}