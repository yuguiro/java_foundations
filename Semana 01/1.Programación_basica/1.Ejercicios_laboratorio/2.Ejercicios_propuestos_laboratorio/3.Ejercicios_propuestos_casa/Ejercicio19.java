import java.io.*;

public class Ejercicio19 {
    public static void main(String[] args) {
        try {
            OutputStream os = new FileOutputStream("output.txt");
            String data = "Hola, Senatino!";
            byte[] byteArray = data.getBytes();
            os.write(byteArray);
            os.close();
            System.out.println("Los datos se han escrito en el archivo.");
        } catch (IOException e) {
            System.out.println("Se ha producido un error al escribir los datos en el archivo.");
            e.printStackTrace();
        }
    }
}