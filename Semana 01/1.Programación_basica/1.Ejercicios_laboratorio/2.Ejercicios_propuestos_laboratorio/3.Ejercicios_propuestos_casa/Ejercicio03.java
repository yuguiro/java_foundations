import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Ejercicio03 {
    public static void main(String[] args) {
        // Creamos un objeto Date con la fecha y hora actual
        Date fechaHoraActual = new Date();

        // Creamos un objeto SimpleDateFormat con el patrón deseado y el local en idioma español.
        SimpleDateFormat formatoFechaHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));

        // Formateamos la fecha y hora en una cadena de texto
        String fechaHoraFormateada = formatoFechaHora.format(fechaHoraActual);

        // Mostramos la cadena que la consola formateó.
        System.out.println("La fecha y hora actual es : " + fechaHoraFormateada);
    }
}