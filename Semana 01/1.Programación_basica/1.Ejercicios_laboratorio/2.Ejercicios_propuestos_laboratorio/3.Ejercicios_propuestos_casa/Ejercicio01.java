import java.util.ArrayList;

public class Ejercicio01 {
    public static void main(String[] args) {
        ArrayList<String> nombres = new ArrayList<String>();
        nombres.add("Jakeline");
        nombres.add("Ivalessa");
        nombres.add("Jhandy");
        nombres.add("Judith");
        System.out.println("Los nombres son: " + nombres);
    }
}