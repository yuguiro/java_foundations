import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ejercicio11 {
    public static void main(String[] args) {
        // Creamos un patrón para buscar una secuencia de caracteres específica
        String patronTexto = "Java";
        Pattern patron = Pattern.compile(patronTexto);

        // Leemos la cadena de texto desde la consola
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese una cadena de texto:");
        String texto = scanner.nextLine();

        // Utilizamos el patrón para buscar en la cadena de texto
        Matcher matcher = patron.matcher(texto);
        boolean resultado = matcher.find();

        // Imprimimos el resultado como "Sí" o "No"
        if (resultado) {
            System.out.println("Sí, el patrón \"" + patronTexto + "\" se encuentra en el texto.");
        } else {
            System.out.println("No, el patrón \"" + patronTexto + "\" no se encuentra en el texto.");
        }
    }
}
