import java.util.Locale;

public class Ejercicio17 {
    public static void main(String[] args) {
        // Creamos un objeto Locale con la configuración regional deseada
        Locale configuracionRegional = new Locale("español", "ESPAÑOL");

        // Imprimimos la información de la configuración regional
        System.out.println("El idioma es: " + configuracionRegional.getLanguage());
        System.out.println("El país es: " + configuracionRegional.getCountry());
    }
}