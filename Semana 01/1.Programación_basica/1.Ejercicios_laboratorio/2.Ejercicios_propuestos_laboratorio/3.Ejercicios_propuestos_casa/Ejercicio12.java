import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Ejercicio12 {
    public static void main(String[] args) {
        // Creamos un objeto Date con la fecha y hora actual
        Date fechaActual = new Date();

        // Creamos un objeto DateFormat para dar formato a la fecha y hora
        DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");

        // Obtenenemos la fecha y hora formateadas
        String fechaFormateada = formatoFecha.format(fechaActual);
        String horaFormateada = formatoHora.format(fechaActual);

        // Imprimimos la fecha y hora formateadas
        System.out.println("La Fecha es: " + fechaFormateada);
        System.out.println("La Hora es: " + horaFormateada);
    }
}
