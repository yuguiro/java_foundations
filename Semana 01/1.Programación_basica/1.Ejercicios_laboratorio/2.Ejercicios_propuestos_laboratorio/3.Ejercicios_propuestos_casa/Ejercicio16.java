import java.util.TimeZone;

public class Ejercicio16 {
    public static void main(String[] args) {
        // Creamos un objeto TimeZone con la zona horaria deseada
        TimeZone zonaHoraria = TimeZone.getTimeZone("America/Lima");

        // Imprimimos la información de la zona horaria
        System.out.println("La zona horaria es: " + zonaHoraria.getID());
        System.out.println("El desplazamiento de la zona horaria es: " + zonaHoraria.getRawOffset() / 3600000 + " horas");
    }
}
