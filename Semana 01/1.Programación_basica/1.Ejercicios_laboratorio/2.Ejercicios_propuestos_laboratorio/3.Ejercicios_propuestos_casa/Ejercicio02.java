import java.util.HashMap;

public class Ejercicio02 {
    public static void main(String[] args) {
        HashMap<String, Integer> nombresEdades = new HashMap<String, Integer>();
        nombresEdades.put("Jakeline", 32);
        nombresEdades.put("Ivalessa", 7);
        nombresEdades.put("Jhandy", 12);
        nombresEdades.put("Judith", 42);
        System.out.println("Los nombres son y sus edades son: " + nombresEdades);
    }
}