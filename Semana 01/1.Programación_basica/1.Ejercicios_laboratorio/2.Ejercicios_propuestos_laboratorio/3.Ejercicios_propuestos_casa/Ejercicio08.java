import java.util.Random;

public class Ejercicio08 {
    public static void main(String[] args) {
        //Creamos una instancia de la clase Random
        Random rand = new Random();

        //Generamos varios números aleatorios entre 0 y 9
        for (int i = 0; i < 3; i++) {
            int numeroAleatorio = rand.nextInt(100);
            System.out.println("Número aleatorio " + (i+1) + ": " + numeroAleatorio);
        }

    }
}
