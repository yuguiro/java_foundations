import java.math.BigInteger;

public class Ejercicio15 {
    public static void main(String[] args) {
        // Creamos dos objetos BigInteger con los valores deseados
        BigInteger numero1 = new BigInteger("159753");
        BigInteger numero2 = new BigInteger("456852");

        // Realizamos algunas operaciones basicas de matemáticas con los objetos BigInteger
        BigInteger suma = numero1.add(numero2);
        BigInteger resta = numero1.subtract(numero2);
        BigInteger multiplicacion = numero1.multiply(numero2);
        BigInteger division = numero1.divide(numero2);

        // Imprimimos los resultados
        System.out.println("La Suma de: " + numero1 + "  y  " + numero2 + " = "+ suma);
        System.out.println("La Resta de: " + numero1 + "  y  " + numero2 + " = " + resta);
        System.out.println("La Multiplicación de: " + numero1 + "  y  " + numero2 + " = " + multiplicacion);
        System.out.println("La División de: " + numero1 + "  y  " + numero2 + " = " + division);
    }
}