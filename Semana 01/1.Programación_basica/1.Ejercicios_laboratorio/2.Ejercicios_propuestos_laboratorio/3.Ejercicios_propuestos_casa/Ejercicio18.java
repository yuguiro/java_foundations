import java.io.InputStream;
import java.util.Scanner;

public class Ejercicio18 {
    public static void main(String[] args) {
        // Creamos una variable de tipo InputStream para leer datos de entrada
        InputStream entrada = System.in;

        // Creamos un objeto Scanner para leer datos de entrada
        Scanner scanner = new Scanner(entrada);

        // Leemos una cadena de texto ingresada por el usuario
        System.out.print("Ingrese una cadena de texto: ");
        String texto = scanner.nextLine();

        // Imprimimos la cadena de texto ingresada
        System.out.println("La cadena de texto ingresada es: " + texto);
    }
}
