import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Ejercicio04 {
    public static void main(String[] args) {
        Calendar fechaActual = Calendar.getInstance();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", new Locale("es", "ES"));
        String fechaFormateada = formatoFecha.format(fechaActual.getTime());
        System.out.println("La fecha actual es: " + fechaFormateada);
    }
}
