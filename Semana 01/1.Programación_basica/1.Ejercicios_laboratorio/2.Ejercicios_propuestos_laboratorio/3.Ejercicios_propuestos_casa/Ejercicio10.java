import java.util.regex.Pattern;

public class Ejercicio10 {
    public static void main(String[] args) {
        // Creamos un patrón para buscar una secuencia de caracteres específica
        Pattern patron = Pattern.compile("Senati");

        // Utilizamos el patrón para buscar en una cadena de texto
        String texto = "Estudiar en Senati es divertido.";
        boolean resultado = patron.matcher(texto).find();

        if (resultado) {
            System.out.println("Sí, el patrón se encontró en el texto.");
        } else {
            System.out.println("No, el patrón no se encontró en el texto.");
        }
    }
}
