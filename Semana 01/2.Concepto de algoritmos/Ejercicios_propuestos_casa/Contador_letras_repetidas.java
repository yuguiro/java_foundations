import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Contador_letras_repetidas {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<Character, Integer> letrasFrecuencia = new HashMap<>();

        System.out.print("Ingrese una palabra: ");
        String palabra = scanner.nextLine();

        while (!palabra.equals("salir")) {
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                if (letrasFrecuencia.containsKey(letra)) {
                    letrasFrecuencia.put(letra, letrasFrecuencia.get(letra) + 1);
                } else {
                    letrasFrecuencia.put(letra, 1);
                }
            }

            System.out.print("Ingrese otra palabra (o 'salir' para terminar): ");
            palabra = scanner.nextLine();
        }

        int letrasRepetidas = 0;
        for (Map.Entry<Character, Integer> entry : letrasFrecuencia.entrySet()) {
            if (entry.getValue() > 1) {
                letrasRepetidas++;
            }
        }

        System.out.println("Existe " + letrasRepetidas + " letras que se repiten en las palabras ingresadas.");
    }
}
