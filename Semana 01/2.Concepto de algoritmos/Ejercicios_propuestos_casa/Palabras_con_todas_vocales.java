import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Palabras_con_todas_vocales {
    public static void main(String[] args) {
        List<String> palabras = Arrays.asList("Comunicarse", "Murcielago", "Desvinculados", "Autoestima");

        int contador = 0;

        // Iterar sobre cada palabra de la lista
        for (String palabra : palabras) {
            Set<Character> vocales = new HashSet<>();

            // Iterar sobre cada letra de la palabra y agregar las vocales al conjunto
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                if (esVocal(letra)) {
                    vocales.add(letra);
                }
            }

            // Comprobar si el conjunto contiene todas las vocales
            if (vocales.size() == 5) {
                contador++;
            }
        }

        System.out.println(contador + " palabras contienen todas las vocales.");
    }

    // Función para comprobar si un caracter es una vocal
    public static boolean esVocal(char c) {
        return "aeiouAEIOU".indexOf(c) != -1;
    }
}
 