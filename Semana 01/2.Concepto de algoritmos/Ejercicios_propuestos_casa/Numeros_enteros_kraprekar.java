public class Numeros_enteros_kraprekar {
    public static void main(String[] args) {
        int contador = 0;
        
        for (int i = 1; i <= 2000; i++) {
            if (esNumeroKaprekar(i)) {
                contador++;
            }
        }
        
        System.out.println("Hay " + contador + " números de Kaprekar entre 1 y 1000.");
    }
    
    public static boolean esNumeroKaprekar(int n) {
        int cuadrado = n * n;
        String cuadradoStr = Integer.toString(cuadrado);
        int longitud = cuadradoStr.length();
        
        for (int i = 1; i < longitud; i++) {
            String primeraParteStr = cuadradoStr.substring(0, i);
            String segundaParteStr = cuadradoStr.substring(i);
            
            int primeraParte = Integer.parseInt(primeraParteStr);
            int segundaParte = Integer.parseInt(segundaParteStr);
            
            if (segundaParte > 0 && primeraParte + segundaParte == n) {
                return true;
            }
        }
        
        return false;
    }
}
