public class Numeros_narcisismo {
    public static void main(String[] args) {
        int[] numeros = {159, 753, 456, 852, 1634, 8208, 9474, 9475, 9476};
        int contadorNarcisismo = 0;

        for (int numero : numeros) {
            if (esNumeroNarcisismo(numero)) {
                contadorNarcisismo++;
            }
        }

        System.out.println("Existen " + contadorNarcisismo + " numeros de narcisismo en el conjunto.");
    }

    public static boolean esNumeroNarcisismo(int numero) {
        String numeroComoCadena = Integer.toString(numero);
        int potencia = numeroComoCadena.length();
        int suma = 0;

        for (int i = 0; i < potencia; i++) {
            int digito = Integer.parseInt(Character.toString(numeroComoCadena.charAt(i)));
            suma += Math.pow(digito, potencia);
        }

        return suma == numero;
    }
}
