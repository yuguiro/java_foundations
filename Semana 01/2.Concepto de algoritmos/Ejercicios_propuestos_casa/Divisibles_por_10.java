public class Divisibles_por_10 {
    public static void main(String[] args) {
        int contador = 0;

        // Iterar sobre cada número entero del 1 al 10
        for (int i = 1; i <= 10; i++) {
            // Iterar sobre cada número entero del 1 al 100000 y comprobar si es divisible por i
            for (int j = 1; j <= 100000; j++) {
                if (j % i != 0) {
                    continue; // si no es divisible por i, pasamos al siguiente número
                }

                // Si hemos llegado hasta aquí, el número es divisible por i
                // Si i es igual a 10, incrementamos el contador y pasamos al siguiente número
                if (i == 10) {
                    contador++;
                    continue;
                }

                // Si i no es igual a 10, comprobamos si el número es divisible por el siguiente número
                // Si no lo es, pasamos al siguiente número
                if (j % (i + 1) != 0) {
                    continue;
                }

                // Repetimos el proceso para los números restantes del 1 al 10
                if (i == 9) {
                    contador++;
                    continue;
                }
                if (j % (i + 2) != 0) {
                    continue;
                }
                if (i == 8) {
                    contador++;
                    continue;
                }
                if (j % (i + 3) != 0) {
                    continue;
                }
                if (i == 7) {
                    contador++;
                    continue;
                }
                if (j % (i + 4) != 0) {
                    continue;
                }
                if (i == 6) {
                    contador++;
                    continue;
                }
                if (j % (i + 5) != 0) {
                    continue;
                }
                if (i == 5) {
                    contador++;
                    continue;
                }
                if (j % (i + 6) != 0) {
                    continue;
                }
                if (i == 4) {
                    contador++;
                    continue;
                }
                if (j % (i + 7) != 0) {
                    continue;
                }
                if (i == 3) {
                    contador++;
                    continue;
                }
                if (j % (i + 8) != 0) {
                    continue;
                }
                if (i == 2) {
                    contador++;
                    continue;
                }
                if (j % (i + 9) != 0) {
                    continue;
                }
                if (i == 1) {
                    contador++;
                    continue;
                }
            }
        }

        System.out.println("Hay " + contador + " números divisibles por todos los números de 1 a 10.");
    }
}
