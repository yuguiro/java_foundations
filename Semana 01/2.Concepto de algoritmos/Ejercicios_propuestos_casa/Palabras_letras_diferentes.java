import java.util.Scanner;

public class Palabras_letras_diferentes {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa una lista de palabras separadas por comas: ");
        String listaPalabras = scanner.nextLine();

        String[] palabras = listaPalabras.split(",");

        int contadorPalabrasLetrasDiferentes = 0;

        for (String palabra : palabras) {
            if (tieneLetrasDiferentes(palabra)) {
                contadorPalabrasLetrasDiferentes++;
            }
        }

        System.out.println("Hay " + contadorPalabrasLetrasDiferentes + " palabras con todas sus letras diferentes en la lista.");
    }

    public static boolean tieneLetrasDiferentes(String palabra) {
        for (int i = 0; i < palabra.length() - 1; i++) {
            for (int j = i + 1; j < palabra.length(); j++) {
                if (palabra.charAt(i) == palabra.charAt(j)) {
                    return false;
                }
            }
        }

        return true;
    }
}
