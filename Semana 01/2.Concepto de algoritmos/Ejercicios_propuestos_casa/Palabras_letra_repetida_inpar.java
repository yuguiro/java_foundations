import java.util.Scanner;

public class Palabras_letra_repetida_inpar {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int contador = 0;

        System.out.print("Ingrese una palabra o frase: ");
        String entrada = scanner.nextLine();

        String[] palabras = entrada.split("\\s+");

        for (String palabra : palabras) {
            int[] contadorLetras = new int[26]; // hay 26 letras en el alfabeto inglés

            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                if (Character.isLetter(letra)) { // ignorar caracteres que no sean letras
                    contadorLetras[Character.toLowerCase(letra) - 'a']++;
                }
            }

            // Verificar si alguna letra aparece un número impar de veces
            boolean tieneLetraRepetidaImpar = false;
            for (int contadorLetra : contadorLetras) {
                if (contadorLetra % 2 == 1) {
                    tieneLetraRepetidaImpar = true;
                    break;
                }
            }

            // Si se cumple la condición, aumentar el contador de palabras apropiadas
            if (tieneLetraRepetidaImpar) {
                contador++;
            }
        }

        System.out.println("Se encontraron " + contador + " palabras con una letra repetida un número impar de veces.");
    }
}
