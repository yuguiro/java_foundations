import java.util.Scanner;

public class Numeros_smith {
    public static int[] factorizar(int n) {
        int[] factores = new int[10];
        int i = 2;
        int j = 0;

        while (n > 1) {
            if (n % i == 0) {
                factores[j] = i;
                j++;
                n = n / i;
            } else {
                i++;
            }
        }

        return factores;
    }

    // Función que devuelve la suma de los dígitos de un número
    public static int sumarDigitos(int n) {
        int suma = 0;

        while (n != 0) {
            suma += n % 10;
            n = n / 10;
        }

        return suma;
    }

    // Función que determina si un número es un número de Smith
    public static boolean esNumeroSmith(int n) {
        int sumaDigitos = sumarDigitos(n);
        int[] factores = factorizar(n);
        int sumaFactores = 0;

        // Calcular la suma de los dígitos de la factorización en primos
        for (int i = 0; i < factores.length && factores[i] != 0; i++) {
            sumaFactores += sumarDigitos(factores[i]);
        }

        // Comparar las sumas
        return sumaDigitos == sumaFactores;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese un número: ");
        int n = scanner.nextInt();

        if (esNumeroSmith(n)) {
            System.out.println(n + " si un número de Smith.");
        } else {
            System.out.println(n + " no es un número de Smith.");
        }
    }
}
