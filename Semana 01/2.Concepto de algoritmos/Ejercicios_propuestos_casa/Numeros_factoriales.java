import java.util.Scanner;

public class Numeros_factoriales {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el número máximo a comprobar: ");
        int max = scanner.nextInt();
        int contador = 0;
        
        for (int i = 1; i <= max; i++) {
            int sumaFactoriales = 0;
            int numero = i;
            
            while (numero > 0) {
                int digito = numero % 10;
                sumaFactoriales += factorial(digito);
                numero /= 10;
            }
            
            if (sumaFactoriales == i) {
                contador++;
            }
        }
        
        System.out.println("Se encontraron " + contador + " números que cumplen con la propiedad.");
    }
    
    public static int factorial(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * factorial(n-1);
        }
    }
}
