import java.util.Scanner;

public class Suma_Digito_Divisible {

    public static void main(String[] args) {
        int sumDigitos = 0;

        // Calcular la suma de los dígitos de 1 a 100
        for (int i = 1; i <= 100; i++) {
            int num = i;
            while (num > 0) {
                sumDigitos += num % 10;
                num /= 10;
            }
        }

        // Pedir al usuario que ingrese un número
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int numUsuario = scanner.nextInt();

        // Calcular la suma de los dígitos del número ingresado
        int sumDigitosUsuario = 0;
        int numTemp = numUsuario;
        while (numTemp > 0) {
            sumDigitosUsuario += numTemp % 10;
            numTemp /= 10;
        }

        // Verificar si el número ingresado es divisible por la suma de los dígitos de 1 a 100
        if (numUsuario % sumDigitos == 0) {
            System.out.println(numUsuario + " es divisible por la suma de los dígitos de 1 a 100.");
        } else {
            System.out.println(numUsuario + " no es divisible por la suma de los dígitos de 1 a 100.");
        }
    }
}
