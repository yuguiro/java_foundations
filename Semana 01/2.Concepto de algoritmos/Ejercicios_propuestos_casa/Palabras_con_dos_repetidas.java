public class Palabras_con_dos_repetidas {
    public static void main(String[] args) {
        String[] palabras = {"abracadabra", "alambrada", "desenbemdecerse", "excelentemente", "odontologo", "protocolo"};

        int contador = 0; 

        for (String palabra : palabras) {
            boolean tieneDosRepetidas = false; 
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                if (palabra.lastIndexOf(letra) != i) {
                    if (palabra.indexOf(letra) == i) {
                        tieneDosRepetidas = true; 
                        break; 
                    }
                }
            }
            if (tieneDosRepetidas) {
                contador++; 
            }
        }

        System.out.println("Existen " + contador + " palabras con una letra que aparece exactamente dos veces en cualquier posición");
    }
}
