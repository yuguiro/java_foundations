public class Numero_divisible_20 {
    public static void main(String[] args) {
        int num = 1;
        while (true) {
            boolean esDivisiblePorTodos = true;
            for (int i = 1; i <= 20; i++) {
                if (num % i != 0) {
                    esDivisiblePorTodos = false;
                    break;
                }
            }
            if (esDivisiblePorTodos) {
                System.out.println(num + " es divisible por todos los numeros de 1 a 20.");
                break;
            }
            num++;
        }
    }
}
