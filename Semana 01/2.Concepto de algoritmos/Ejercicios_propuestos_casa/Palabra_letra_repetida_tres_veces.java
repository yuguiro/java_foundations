import java.util.Scanner;

public class Palabra_letra_repetida_tres_veces {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int contador = 0;

        System.out.print("Ingrese una palabra o frase: ");
        String entrada = scanner.nextLine();

        // Dividir la entrada en palabras individuales
        String[] palabras = entrada.split("\\s+");

        // Para cada palabra, verificar si tiene una letra repetida tres veces consecutivas
        for (String palabra : palabras) {
            boolean tieneLetraRepetida = false;

            for (int i = 0; i < palabra.length() - 2; i++) {
                if (palabra.charAt(i) == palabra.charAt(i+1) && palabra.charAt(i+1) == palabra.charAt(i+2)) {
                    // Verificar si todas las letras son iguales
                    char letraRepetida = palabra.charAt(i);
                    boolean todasLasLetrasSonIguales = true;
                    for (int j = i+3; j < palabra.length(); j++) {
                        if (palabra.charAt(j) != letraRepetida) {
                            todasLasLetrasSonIguales = false;
                            break;
                        }
                    }
                    if (todasLasLetrasSonIguales) {
                        tieneLetraRepetida = true;
                        break;
                    }
                }
            }

            // Si se cumple la condición, aumentar el contador de palabras apropiadas
            if (tieneLetraRepetida) {
                contador++;
            }
        }

        System.out.println("Se encontraron " + contador + " palabras con la misma letra repetida tres veces consecutivas.");
    }
}
