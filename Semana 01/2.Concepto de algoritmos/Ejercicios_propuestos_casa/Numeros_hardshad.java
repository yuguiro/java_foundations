public class Numeros_hardshad {
    public static void main(String[] args) {
        int maximo = 1000; // Número máximo a comprobar

        int contador = 0; // Contador de números de Harshad encontrados

        for (int i = 1; i <= maximo; i++) {
            int sumaDigitos = sumaDigitos(i); // Suma de los dígitos del número
            if (i % sumaDigitos == 0) {
                contador++; // El número es un número de Harshad
            }
        }

        System.out.println("Se encontraron " + contador + " números de Harshad en el rango de 1 a " + maximo);
    }

    // Función para calcular la suma de los dígitos de un número
    public static int sumaDigitos(int numero) {
        int suma = 0;
        while (numero > 0) {
            suma += numero % 10; // Sumar el último dígito del número
            numero /= 10; // Eliminar el último dígito del número
        }
        return suma;
    }
}
