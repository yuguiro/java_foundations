import java.util.Scanner;

public class EJE_18_LA {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la longitud máxima permitida: ");
        int longitudMaxima = sc.nextInt();
        System.out.print("Ingrese la cantidad de palabras: ");
        int cantidadPalabras = sc.nextInt();
        sc.nextLine(); // Consumir el salto de línea pendiente
        
        int contadorPalabrasCortas = 0;
        for (int i = 1; i <= cantidadPalabras; i++) {
            System.out.print("Ingrese la palabra #" + i + ": ");
            String palabra = sc.nextLine();
            if (palabra.length() < longitudMaxima) {
                contadorPalabrasCortas++;
            }
        }
        System.out.println("La cantidad de palabras con longitud menor a " + longitudMaxima + " es: " + contadorPalabrasCortas);
    }
}
