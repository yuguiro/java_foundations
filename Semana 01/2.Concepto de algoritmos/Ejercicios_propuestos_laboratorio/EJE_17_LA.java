import java.util.Scanner;
import java.util.ArrayList;

public class EJE_17_LA {
    public static void main(String[] args) {
        ArrayList<String> palabras = new ArrayList<>();
        Scanner input = new Scanner(System.in);

        // Leer la lista de palabras
        System.out.print("Ingrese la lista de palabras separadas por espacio: ");
        String[] listaPalabras = input.nextLine().split(" ");

        // Agregar las palabras a la lista
        for (String palabra : listaPalabras) {
            palabras.add(palabra);
  
           }   }
}
