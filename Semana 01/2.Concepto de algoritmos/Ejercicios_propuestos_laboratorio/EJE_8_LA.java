import java.util.Scanner;

public class EJE_8_LA {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese una fecha en formato dd/mm/yyyy: ");
        String fecha = scanner.nextLine();

        String[] partes = fecha.split("/");
        int dia = Integer.parseInt(partes[0]);
        int mes = Integer.parseInt(partes[1]);
        int anio = Integer.parseInt(partes[2]);

        boolean bisiesto = ((anio % 4 == 0) && (anio % 100 != 0) || (anio % 400 == 0));

        int numDias = 0;
        if (mes == 2) {
            if (bisiesto) {
                numDias = 29;
            } else {
                numDias = 28;
            }
        } else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
            numDias = 30;
        } else {
            numDias = 31;
        }
        boolean fechaValida = (dia > 0 && dia <= numDias);

        
        if (fechaValida) {
            System.out.println("La fecha " + fecha + " es válida.");
        } else {
            System.out.println("La fecha " + fecha + " es inválida.");
        }
    }
}
