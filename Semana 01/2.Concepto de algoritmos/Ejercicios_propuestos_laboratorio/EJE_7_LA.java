import java.util.Scanner;

public class EJE_7_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese una cadena de caracteres: ");
        String cadena = ingreso.nextLine();

        cadena = cadena.replaceAll("\\s", "").toLowerCase();

        int i = 0;
        int j = cadena.length() - 1;
        while (i < j) {
            if (cadena.charAt(i) != cadena.charAt(j)) {
                System.out.println(cadena + " no es un palíndromo");
                return;
            }
            i++;
            j--;
        }

        System.out.println(cadena + " es un palíndromo");
    }
}
