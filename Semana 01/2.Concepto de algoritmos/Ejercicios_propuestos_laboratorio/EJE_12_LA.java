import java.util.Scanner;

public class EJE_12_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese una lista de números separados por comas: ");
        String input = ingreso.nextLine();

        String[] numerosStr = input.split(",");
        int[] numeros = new int[numerosStr.length];
        int contadorPerfectos = 0;

        for (int i = 0; i < numerosStr.length; i++) {
            numeros[i] = Integer.parseInt(numerosStr[i].trim());
            int sumaDivisores = 0;
            for (int j = 1; j < numeros[i]; j++) {
                if (numeros[i] % j == 0) {
                    sumaDivisores += j;
                }
            }
            if (sumaDivisores == numeros[i]) {
                contadorPerfectos++;
            }
        }
        System.out.println("Hay " + contadorPerfectos + " números perfectos en la lista.");
    }
}
