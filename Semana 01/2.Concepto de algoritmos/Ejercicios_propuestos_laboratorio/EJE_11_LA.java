import java.util.Scanner;;
public class EJE_11_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de números a evaluar: ");
        int cantidadNumeros = ingreso.nextInt();
        int cantidadMultiplosDe3 = 0;

        for (int i = 1; i <= cantidadNumeros; i++) {
            System.out.print("Ingrese el número " + i + ": ");
            int numero = ingreso.nextInt();

            if (numero % 3 == 0) {
                cantidadMultiplosDe3++;
            }
        }

        System.out.println("La cantidad de números múltiplos de 3 es: " + cantidadMultiplosDe3);
    }
}
