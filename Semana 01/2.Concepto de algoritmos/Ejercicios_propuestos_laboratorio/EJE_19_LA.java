
import java.util.Scanner;
public class EJE_19_LA {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un numero entero: ");
        int n = sc.nextInt();
        double suma = 0.0;
        for (int i = 1; i <= n; i++) {
            suma += 1.0 / i;
        }
        if (suma == n) {
            System.out.println(n + " es un numero armonico.");
        } else {
            System.out.println(n + " no es un numero armonico.");
        }
        sc.close();
    }
}
